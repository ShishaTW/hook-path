<?php

namespace Hooks\Facades;

use Hooks\Facades\Facade;

class Path extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'path';
    }
}
