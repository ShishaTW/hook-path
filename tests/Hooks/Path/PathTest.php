<?php

namespace Test\Hooks\Path;

use PHPUnit\Framework\TestCase;

use Hooks\Path\Path;

class PathTest extends TestCase
{
    public function testPath()
    {
        $path = new Path(__DIR__);

        $this->assertEquals(__DIR__, $path->root());
        $this->assertEquals(
            __DIR__ . DIRECTORY_SEPARATOR . 'filename',
            $path->root('filename')
        );
    }
}
