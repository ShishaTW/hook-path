<?php

namespace Hooks\Path;

use Hooks\Path\Path;
use Hooks\ServiceProvider;

class PathServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('path', function ($app) {
            return new Path($app['config']['repository']);
        });
    }

    public function boot()
    {
        //
    }
}
