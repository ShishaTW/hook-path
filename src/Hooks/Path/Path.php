<?php

namespace Hooks\Path;

use ReflectionClass;
use Composer\Autoload\ClassLoader;

class Path
{
    /**
     * 根目錄路徑
     *
     * @var string
     */
    protected $rootPath;

    /**
     *
     * @param strin $rootPath 手動指定一個路徑
     */
    public function __construct($rootPath = null)
    {
        $this->rootPath = rtrim($rootPath, '\\/') ?: $this->resolveRootPath();
    }

    /**
     * 解析專案根目錄。透過 ReflectionClass 找到 Composer 的真實路徑，再反推出根目錄。
     *
     * @return string
     */
    protected function resolveRootPath()
    {
        $reflection = new ReflectionClass(ClassLoader::class);

        $classLoaderRealPath = $reflection->getFileName();

        return realpath(dirname($classLoaderRealPath) . '/../..');
    }

    /**
     * 處理路徑前後的目錄分隔符
     *
     * @param  string $path
     * @return string
     */
    public function parsePath($path)
    {
        return trim($path, '\\/');
    }

    /**
     * 取得相對於根目錄的路徑
     *
     * @param  string $path
     * @return string
     */
    public function root($path = null)
    {
        return rtrim(
            $this->rootPath . DIRECTORY_SEPARATOR . ltrim($path, '\\/'),
            '\\/'
        );
    }
}
