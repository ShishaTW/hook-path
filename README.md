# hook-path

hook-path 是基於 `Hooks` 的擴充，用來處理與生成路徑。


## Installation

使用 composer 安裝

```bash
composer config repositories.hooks vcs https://ShishaTW@bitbucket.org/ShishaTW/hook-path.git
composer require "shishamou/hook-path:0.1.*"
```

## Usage

### 範例
取得基於專案目錄的檔案路徑

```php
use Hooks\Facades\Path;

echo Path::root('public/index.php');
```